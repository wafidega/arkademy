@extends('include.master')

@section('content')
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Daftar Produk</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/produk/{{$produk->id}}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Nama Produk</label>
                    <input type="text" class="form-control" id="nama" placeholder="Nama Produk" name="nama" value="{{old('nama', $produk->nama_produk)}}">
                  </div>
                  <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" class="form-control" id="keterangan" placeholder="Keterangan" name="keterangan" value="{{old('keterangan',$produk->keterangan)}}">
                  </div>
                  <div class="form-group">
                    <label for="harga">Harga</label>
                    <input type="text" class="form-control" id="harga" placeholder="Harga" name="harga" value="{{old('harga', $produk->harga)}}">
                  </div><div class="form-group">
                    <label for="tahun">Jumlah</label>
                    <input type="text" class="form-control" id="jumlah" placeholder="Jumlah" name="jumlah" value="{{old('jumlah',$produk->jumlah)}}">
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
@endsection