<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ProdukController extends Controller
{
    public function index() {
        $produk = DB::table('produk')->get();
           // dd($film);
           return view('index', compact('produk'));
        }
    
        public function create() {
            return view('create');
        }
    
        public function store(Request $request) {
           $query = DB::table('produk')->insert([
               "nama_produk" => $request["nama"],
               "keterangan" => $request["keterangan"],
               "harga" => $request["harga"],
               "jumlah" => $request["jumlah"]
           ]);
    
           return redirect('/produk');
        }

        public function show ($id) {
            $produk = DB::table('produk')->where('id', $id)->first();
            // dd($pertanyaan);
            return view('show', compact('produk'));
    }

    public function edit($id) {
        $produk = DB::table('produk')->where('id', $id)->first();
 
        return view('edit', compact('produk'));
    }

    public function update($id, Request $request){
        $query = DB::table('produk')
                    ->where('id', $id)
                    ->update([
                        "nama_produk" => $request["nama"],
                        "keterangan" => $request["keterangan"],
                        "harga" => $request["harga"],
                        "jumlah" => $request["jumlah"]
                    ]);
            return redirect('/produk');
     }

     public function destroy($id) {
         $query = DB::table('produk')->where('id', $id)->delete();
         return redirect('/produk');
     }
       
}
